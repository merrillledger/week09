'use strict';



;define("week-09/adapters/-json-api", ["exports", "@ember-data/adapter/json-api"], function (_exports, _jsonApi) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _jsonApi.default;
    }
  });
});
;define("week-09/adapters/application", ["exports", "ember-data"], function (_exports, _emberData) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = _emberData.default.JSONAPIAdapter.extend({
    host: 'http://localhost:4201'
  });

  _exports.default = _default;
});
;define("week-09/app", ["exports", "ember-resolver", "ember-load-initializers", "week-09/config/environment"], function (_exports, _emberResolver, _emberLoadInitializers, _environment) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var App = Ember.Application.extend({
    modulePrefix: _environment.default.modulePrefix,
    podModulePrefix: _environment.default.podModulePrefix,
    Resolver: _emberResolver.default
  });
  (0, _emberLoadInitializers.default)(App, _environment.default.modulePrefix);
  var _default = App;
  _exports.default = _default;
});
;define("week-09/component-managers/glimmer", ["exports", "@glimmer/component/-private/ember-component-manager"], function (_exports, _emberComponentManager) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _emberComponentManager.default;
    }
  });
});
;define("week-09/components/number-game", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var __COLOCATED_TEMPLATE__ = Ember.HTMLBars.template(
  /*
    {{#if playing}}
      {{#if correct}}
          <div class="text-center">Yes! It only took me {{guesses}} guesses.</div>
          <div class="text-center">Do you want to <button {{action 'start'}}>play again!</button></div>
              <div class="text-center">{{input value=player_name placeholder='Your Name'}}<button {{action 'save-highscore'}}>Save Highscore!</button></div>
      {{else}}
          <div class="text-center">Is it {{guessValue}}?</div>
          <div class="text-center">
              <button {{action 'lower'}}>Lower</button>
              <button {{action 'correct'}}>Correct</button>
              <button {{action 'higher'}}>Higher</button>
          </div>
          <div class="text-center">
              <p>Computer guesses so far: {{guesses}}</p>
              <button {{action 'start'}}>Restart</button>
          </div>
      {{/if}}
  {{else}}
      <div class="text-center">Think of a number between 1 and 100 and then press <button {{action 'start'}}>Start!</button></div>
  {{/if}}
  
  */
  {"id":"Emf7Nvfz","block":"{\"symbols\":[],\"statements\":[[6,[37,6],[[35,7]],null,[[\"default\",\"else\"],[{\"statements\":[[6,[37,6],[[35,5]],null,[[\"default\",\"else\"],[{\"statements\":[[2,\"        \"],[10,\"div\"],[14,0,\"text-center\"],[12],[2,\"Yes! It only took me \"],[1,[34,2]],[2,\" guesses.\"],[13],[2,\"\\n        \"],[10,\"div\"],[14,0,\"text-center\"],[12],[2,\"Do you want to \"],[11,\"button\"],[4,[38,0],[[32,0],\"start\"],null],[12],[2,\"play again!\"],[13],[13],[2,\"\\n            \"],[10,\"div\"],[14,0,\"text-center\"],[12],[1,[30,[36,4],null,[[\"value\",\"placeholder\"],[[35,3],\"Your Name\"]]]],[11,\"button\"],[4,[38,0],[[32,0],\"save-highscore\"],null],[12],[2,\"Save Highscore!\"],[13],[13],[2,\"\\n\"]],\"parameters\":[]},{\"statements\":[[2,\"        \"],[10,\"div\"],[14,0,\"text-center\"],[12],[2,\"Is it \"],[1,[34,1]],[2,\"?\"],[13],[2,\"\\n        \"],[10,\"div\"],[14,0,\"text-center\"],[12],[2,\"\\n            \"],[11,\"button\"],[4,[38,0],[[32,0],\"lower\"],null],[12],[2,\"Lower\"],[13],[2,\"\\n            \"],[11,\"button\"],[4,[38,0],[[32,0],\"correct\"],null],[12],[2,\"Correct\"],[13],[2,\"\\n            \"],[11,\"button\"],[4,[38,0],[[32,0],\"higher\"],null],[12],[2,\"Higher\"],[13],[2,\"\\n        \"],[13],[2,\"\\n        \"],[10,\"div\"],[14,0,\"text-center\"],[12],[2,\"\\n            \"],[10,\"p\"],[12],[2,\"Computer guesses so far: \"],[1,[34,2]],[13],[2,\"\\n            \"],[11,\"button\"],[4,[38,0],[[32,0],\"start\"],null],[12],[2,\"Restart\"],[13],[2,\"\\n        \"],[13],[2,\"\\n\"]],\"parameters\":[]}]]]],\"parameters\":[]},{\"statements\":[[2,\"    \"],[10,\"div\"],[14,0,\"text-center\"],[12],[2,\"Think of a number between 1 and 100 and then press \"],[11,\"button\"],[4,[38,0],[[32,0],\"start\"],null],[12],[2,\"Start!\"],[13],[13],[2,\"\\n\"]],\"parameters\":[]}]]]],\"hasEval\":false,\"upvars\":[\"action\",\"guessValue\",\"guesses\",\"player_name\",\"input\",\"correct\",\"if\",\"playing\"]}","meta":{"moduleName":"week-09/components/number-game.hbs"}}); // eslint-disable-next-line no-undef
  // eslint-disable-next-line ember/new-module-imports


  var _default = Ember._setComponentTemplate(__COLOCATED_TEMPLATE__, Ember.Component.extend({
    guessValue: 0,
    limits: null,
    playing: false,
    correct: false,
    guesses: 0,
    actions: {
      start: function start() {
        this.set('playing', true);
        this.set('correct', false);
        this.set('guessValue', Math.floor(Math.random() * 100) + 1);
        this.set('guesses', 1);
        this.set('limits', {
          'min': 1,
          'max': 100
        });
      },
      lower: function lower() {
        var limit = this.get('limits');
        limit.max = this.get('guessValue');
        this.set('guessValue', limit.min + Math.floor((limit.max - limit.min) / 2));
        this.set('guesses', this.get('guesses') + 1);
      },
      higher: function higher() {
        var limit = this.get('limits');
        limit.min = this.get('guessValue');
        this.set('guessValue', limit.min + Math.floor((limit.max - limit.min + 1) / 2));
        this.set('guesses', this.get('guesses') + 1);
      },
      correct: function correct() {
        this.set('correct', true);
      },
      'save-highscore': function saveHighscore() {
        var action = this.get('on-save-highscore');

        if (action !== undefined) {
          action(this.get('player_name'), this.get('guesses'));
        }
      }
    }
  }));

  _exports.default = _default;
});
;define("week-09/components/tic-tac-toe", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var __COLOCATED_TEMPLATE__ = Ember.HTMLBars.template(
  /*
    {{#if playing}}
    {{#if winner}}  
      <div>
        Player {{winner}} won!
      </div>
    {{/if}}
    {{#if draw}}
        We'll call it a draw.
    {{/if}}
    {{#if desktop}}
      <button {{action 'start'}}>Restart</button>
    {{/if}}
  {{else}}
    {{#if desktop}}
      <button {{action 'start'}}>Start</button>
    {{/if}}
  {{/if}}
  <canvas id='stage' width="380" height="380"> </canvas>
  
  */
  {"id":"PSytr7f9","block":"{\"symbols\":[],\"statements\":[[6,[37,2],[[35,5]],null,[[\"default\",\"else\"],[{\"statements\":[[6,[37,2],[[35,3]],null,[[\"default\"],[{\"statements\":[[2,\"    \"],[10,\"div\"],[12],[2,\"\\n      Player \"],[1,[34,3]],[2,\" won!\\n    \"],[13],[2,\"\\n\"]],\"parameters\":[]}]]],[6,[37,2],[[35,4]],null,[[\"default\"],[{\"statements\":[[2,\"      We'll call it a draw.\\n\"]],\"parameters\":[]}]]],[6,[37,2],[[35,1]],null,[[\"default\"],[{\"statements\":[[2,\"    \"],[11,\"button\"],[4,[38,0],[[32,0],\"start\"],null],[12],[2,\"Restart\"],[13],[2,\"\\n\"]],\"parameters\":[]}]]]],\"parameters\":[]},{\"statements\":[[6,[37,2],[[35,1]],null,[[\"default\"],[{\"statements\":[[2,\"    \"],[11,\"button\"],[4,[38,0],[[32,0],\"start\"],null],[12],[2,\"Start\"],[13],[2,\"\\n\"]],\"parameters\":[]}]]]],\"parameters\":[]}]]],[10,\"canvas\"],[14,1,\"stage\"],[14,\"width\",\"380\"],[14,\"height\",\"380\"],[12],[2,\" \"],[13],[2,\"\\n\"]],\"hasEval\":false,\"upvars\":[\"action\",\"desktop\",\"if\",\"winner\",\"draw\",\"playing\"]}","meta":{"moduleName":"week-09/components/tic-tac-toe.hbs"}});

  var _default = Ember._setComponentTemplate(__COLOCATED_TEMPLATE__, Ember.Component.extend({
    playing: false,
    winner: undefined,
    draw: false,
    desktop: true,
    init: function init() {
      this._super.apply(this, arguments);

      createjs.Sound.registerSound("assets/sounds/click.wav", "place-marker");
      createjs.Sound.registerSound("assets/sounds/falling.mp3", "falling");
      createjs.Sound.registerSound("assets/sounds/cheer.wav", "cheer");
      var component = this;
      document.addEventListener("deviceready", function () {
        component.set('desktop', false);

        if (shake) {
          shake.startWatch(function () {
            component.send('start');
          });
        }
      }, false);
    },
    didInsertElement: function didInsertElement() {
      var stage = new createjs.Stage(this.element.querySelector("#stage")); //draw the game board

      var board = new createjs.Shape();
      var graphics = board.graphics;
      graphics.beginFill("#ffffff");
      graphics.drawRect(0, 99, 300, 2);
      graphics.drawRect(0, 199, 300, 2);
      graphics.drawRect(99, 0, 2, 300);
      graphics.drawRect(199, 0, 2, 300);
      board.x = 40;
      board.y = 40;
      board.alpha = 0;
      this.set('board', board);
      stage.addChild(board);
      var markers = {
        'x': [],
        'o': []
      };

      for (var x = 0; x < 5; x++) {
        var circleMarker = new createjs.Shape();
        graphics = circleMarker.graphics;
        graphics.beginStroke("#66ff66");
        graphics.setStrokeStyle(10);
        graphics.drawCircle(0, 0, 30);
        circleMarker.visible = false;
        stage.addChild(circleMarker);
        markers.o.push(circleMarker);
        var crossMarker = new createjs.Shape();
        graphics = crossMarker.graphics;
        graphics.beginStroke("#6666ff");
        graphics.moveTo(0, 0);
        graphics.lineTo(40, 40);
        graphics.moveTo(0, 40);
        graphics.lineTo(40, 0);
        crossMarker.visible = false;
        stage.addChild(crossMarker);
        markers.x.push(crossMarker);
      }

      this.set("markers", markers);
      this.set("stage", stage);
      createjs.Ticker.addEventListener("tick", stage);
    },
    willDestroyElement: function willDestroyElement() {
      this._super.apply(this, arguments);

      if (shake) {
        shake.stopWatch();
      }
    },
    click: function click(ev) {
      if (this.get("playing") && !this.get("winner")) {
        if (ev.target.tagName.toLowerCase() === "canvas" && ev.offsetX >= 40 && ev.offsetY >= 40 && ev.offsetX < 340 && ev.offsetY < 340) {
          var x = Math.floor((ev.offsetX - 40) / 100);
          var y = Math.floor((ev.offsetY - 40) / 100);
          var state = this.get("state");

          if (!state[x][y]) {
            createjs.Sound.play("place-marker");

            var _player = this.get("player");

            state[x][y] = _player;

            var move_count = this.get("moves")[_player];

            var marker = this.get("markers")[_player][move_count];

            marker.visible = true;

            if (_player == "x") {
              marker.x = 70 + x * 100;
              marker.y = 70 + y * 100;
            } else {
              marker.x = 90 + x * 100;
              marker.y = 90 + y * 100;
            }

            this.check_winner();
            this.get("moves")[_player] = move_count + 1;

            if (_player == "x") {
              this.set("player", "o");
            } else {
              this.set("player", "x");
            }

            this.get("stage").update();
          }
        }
      }
    },
    check_winner: function check_winner() {
      var patterns = [[[0, 0], [1, 1], [2, 2]], [[0, 2], [1, 1], [2, 0]], [[0, 0], [0, 1], [0, 2]], [[1, 0], [1, 1], [1, 2]], [[2, 0], [2, 1], [2, 2]], [[0, 0], [1, 0], [2, 0]], [[0, 1], [1, 1], [2, 1]], [[0, 2], [1, 2], [2, 2]]];
      var state = this.get('state');

      for (var pidx = 0; pidx < patterns.length; pidx++) {
        var pattern = patterns[pidx];
        var winner = state[pattern[0][0]][pattern[0][1]];

        if (winner) {
          for (var idx = 1; idx < pattern.length; idx++) {
            if (winner != state[pattern[idx][0]][pattern[idx][1]]) {
              winner = undefined;
              break;
            }
          }

          if (winner) {
            this.set('winner', winner);
            createjs.Sound.play("cheer");
            break;
          }
        }
      }

      if (!this.get('winner')) {
        var draw = true;

        for (var x = 0; x <= 2; x++) {
          for (var y = 0; y <= 2; y++) {
            if (!state[x][y]) {
              draw = false;
              break;
            }
          }
        }

        this.set('draw', draw);
      }
    },
    actions: {
      start: function start() {
        var board = this.get('board');
        board.alpha = 0;

        if (this.get('playing')) {
          var markers = this.get('markers');

          for (var idx = 0; idx < 5; idx++) {
            createjs.Tween.get(markers.x[idx]).to({
              y: 600
            }, 500);
            createjs.Tween.get(markers.o[idx]).to({
              y: 600
            }, 500);
          }

          createjs.Sound.play("falling");
          createjs.Tween.get(board).wait(500).to({
            alpha: 1
          }, 1000);
        } else {
          createjs.Tween.get(board).to({
            alpha: 1
          }, 1000);
        }

        this.set("playing", true);
        this.set("winner", undefined);
        this.set("draw", false);
        this.set("state", [[undefined, undefined, undefined], [undefined, undefined, undefined], [undefined, undefined, undefined]]);
        this.set("moves", {
          'x': 0,
          'o': 0
        });
        this.set("player", "x");
        this.get("stage").update();

        if (window.plugins && window.plugins.toast) {
          window.plugins.toast.showShortBottom('X to play next');
        }

        if (player == 'x') {
          this.set('player', 'o');
        } else {
          this.set('player', 'x');
        }

        if (this.get('winner') && window.plugins.toast) {
          window.plugins.toast.showShortBottom(this.get('player').toUpperCase() + ' to play next');
        }
      }
    }
  }));

  _exports.default = _default;
});
;define("week-09/components/welcome-page", ["exports", "ember-welcome-page/components/welcome-page"], function (_exports, _welcomePage) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _welcomePage.default;
    }
  });
});
;define("week-09/controllers/game", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.Controller.extend({
    actions: {
      "save-highscore": function saveHighscore(name, score) {
        var controller = this;
        var highscore = controller.store.createRecord("highscore", {
          name: name,
          score: score
        });
        highscore.save().then(function () {
          controller.store.unloadAll();
          controller.transitionToRoute("highscores");
        });
      }
    }
  });

  _exports.default = _default;
});
;define("week-09/data-adapter", ["exports", "@ember-data/debug"], function (_exports, _debug) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _debug.default;
    }
  });
});
;define("week-09/helpers/app-version", ["exports", "week-09/config/environment", "ember-cli-app-version/utils/regexp"], function (_exports, _environment, _regexp) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.appVersion = appVersion;
  _exports.default = void 0;

  function appVersion(_) {
    var hash = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var version = _environment.default.APP.version; // e.g. 1.0.0-alpha.1+4jds75hf
    // Allow use of 'hideSha' and 'hideVersion' For backwards compatibility

    var versionOnly = hash.versionOnly || hash.hideSha;
    var shaOnly = hash.shaOnly || hash.hideVersion;
    var match = null;

    if (versionOnly) {
      if (hash.showExtended) {
        match = version.match(_regexp.versionExtendedRegExp); // 1.0.0-alpha.1
      } // Fallback to just version


      if (!match) {
        match = version.match(_regexp.versionRegExp); // 1.0.0
      }
    }

    if (shaOnly) {
      match = version.match(_regexp.shaRegExp); // 4jds75hf
    }

    return match ? match[0] : version;
  }

  var _default = Ember.Helper.helper(appVersion);

  _exports.default = _default;
});
;define("week-09/helpers/pluralize", ["exports", "ember-inflector/lib/helpers/pluralize"], function (_exports, _pluralize) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var _default = _pluralize.default;
  _exports.default = _default;
});
;define("week-09/helpers/singularize", ["exports", "ember-inflector/lib/helpers/singularize"], function (_exports, _singularize) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var _default = _singularize.default;
  _exports.default = _default;
});
;define("week-09/initializers/app-version", ["exports", "ember-cli-app-version/initializer-factory", "week-09/config/environment"], function (_exports, _initializerFactory, _environment) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var name, version;

  if (_environment.default.APP) {
    name = _environment.default.APP.name;
    version = _environment.default.APP.version;
  }

  var _default = {
    name: 'App Version',
    initialize: (0, _initializerFactory.default)(name, version)
  };
  _exports.default = _default;
});
;define("week-09/initializers/container-debug-adapter", ["exports", "ember-resolver/resolvers/classic/container-debug-adapter"], function (_exports, _containerDebugAdapter) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var _default = {
    name: 'container-debug-adapter',
    initialize: function initialize() {
      var app = arguments[1] || arguments[0];
      app.register('container-debug-adapter:main', _containerDebugAdapter.default);
      app.inject('container-debug-adapter:main', 'namespace', 'application:main');
    }
  };
  _exports.default = _default;
});
;define("week-09/initializers/data-adapter", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `data-adapter` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */
  var _default = {
    name: 'data-adapter',
    before: 'store',
    initialize: function initialize() {}
  };
  _exports.default = _default;
});
;define("week-09/initializers/ember-data-data-adapter", ["exports", "@ember-data/debug/setup"], function (_exports, _setup) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _setup.default;
    }
  });
});
;define("week-09/initializers/ember-data", ["exports", "ember-data", "ember-data/setup-container"], function (_exports, _emberData, _setupContainer) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  /*
    This code initializes EmberData in an Ember application.
  
    It ensures that the `store` service is automatically injected
    as the `store` property on all routes and controllers.
  */
  var _default = {
    name: 'ember-data',
    initialize: _setupContainer.default
  };
  _exports.default = _default;
});
;define("week-09/initializers/export-application-global", ["exports", "week-09/config/environment"], function (_exports, _environment) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.initialize = initialize;
  _exports.default = void 0;

  function initialize() {
    var application = arguments[1] || arguments[0];

    if (_environment.default.exportApplicationGlobal !== false) {
      var theGlobal;

      if (typeof window !== 'undefined') {
        theGlobal = window;
      } else if (typeof global !== 'undefined') {
        theGlobal = global;
      } else if (typeof self !== 'undefined') {
        theGlobal = self;
      } else {
        // no reasonable global, just bail
        return;
      }

      var value = _environment.default.exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = Ember.String.classify(_environment.default.modulePrefix);
      }

      if (!theGlobal[globalName]) {
        theGlobal[globalName] = application;
        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);

            delete theGlobal[globalName];
          }
        });
      }
    }
  }

  var _default = {
    name: 'export-application-global',
    initialize: initialize
  };
  _exports.default = _default;
});
;define("week-09/initializers/injectStore", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `injectStore` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */
  var _default = {
    name: 'injectStore',
    before: 'store',
    initialize: function initialize() {}
  };
  _exports.default = _default;
});
;define("week-09/initializers/store", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `store` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */
  var _default = {
    name: 'store',
    after: 'ember-data',
    initialize: function initialize() {}
  };
  _exports.default = _default;
});
;define("week-09/initializers/transforms", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  /*
    This initializer is here to keep backwards compatibility with code depending
    on the `transforms` initializer (before Ember Data was an addon).
  
    Should be removed for Ember Data 3.x
  */
  var _default = {
    name: 'transforms',
    before: 'store',
    initialize: function initialize() {}
  };
  _exports.default = _default;
});
;define("week-09/instance-initializers/ember-data", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  /* exists only for things that historically used "after" or "before" */
  var _default = {
    name: 'ember-data',
    initialize: function initialize() {}
  };
  _exports.default = _default;
});
;define("week-09/models/highscore", ["exports", "ember-data"], function (_exports, _emberData) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var Model = _emberData.default.Model;

  var _default = Model.extend({
    name: _emberData.default.attr("string"),
    score: _emberData.default.attr("number")
  });

  _exports.default = _default;
});
;define("week-09/resolver", ["exports", "ember-resolver"], function (_exports, _emberResolver) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var _default = _emberResolver.default;
  _exports.default = _default;
});
;define("week-09/router", ["exports", "week-09/config/environment"], function (_exports, _environment) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;
  var Router = Ember.Router.extend({
    location: _environment.default.locationType,
    rootURL: _environment.default.rootURL
  });
  Router.map(function () {
    this.route("game", {
      path: "/"
    });
    this.route('highscores');
  });
  var _default = Router;
  _exports.default = _default;
});
;define("week-09/routes/game", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.Route.extend({});

  _exports.default = _default;
});
;define("week-09/routes/highscores", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.Route.extend({
    model: function model() {
      return this.store.findAll("highscore");
    }
  });

  _exports.default = _default;
});
;define("week-09/serializers/-default", ["exports", "@ember-data/serializer/json"], function (_exports, _json) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _json.default;
    }
  });
});
;define("week-09/serializers/-json-api", ["exports", "@ember-data/serializer/json-api"], function (_exports, _jsonApi) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _jsonApi.default;
    }
  });
});
;define("week-09/serializers/-rest", ["exports", "@ember-data/serializer/rest"], function (_exports, _rest) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _rest.default;
    }
  });
});
;define("week-09/services/store", ["exports", "ember-data/store"], function (_exports, _store) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _store.default;
    }
  });
});
;define("week-09/templates/application", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "7M8Q7VVw",
    "block": "{\"symbols\":[],\"statements\":[[10,\"section\"],[14,1,\"app\"],[12],[2,\"\\n  \"],[10,\"header\"],[12],[2,\"\\n    \"],[10,\"h1\"],[12],[6,[37,0],null,[[\"route\"],[\"game\"]],[[\"default\"],[{\"statements\":[[2,\"Tic-Tac-Toe\"]],\"parameters\":[]}]]],[13],[2,\"\\n  \"],[13],[2,\"\\n  \"],[10,\"article\"],[12],[2,\"\\n    \"],[1,[30,[36,2],[[30,[36,1],null,null]],null]],[2,\"\\n  \"],[13],[2,\"\\n  \"],[10,\"footer\"],[12],[2,\"\\n    \"],[10,\"div\"],[14,0,\"float-left\"],[12],[2,\"\\n      Powered By Ember\\n    \"],[13],[2,\"\\n    \"],[10,\"div\"],[14,0,\"float-right\"],[12],[2,\"\\n      \"],[6,[37,0],null,[[\"route\"],[\"highscores\"]],[[\"default\"],[{\"statements\":[[2,\"High-scores\"]],\"parameters\":[]}]]],[2,\"\\n    \"],[13],[2,\"\\n  \"],[13],[2,\"\\n\"],[13]],\"hasEval\":false,\"upvars\":[\"link-to\",\"-outlet\",\"component\"]}",
    "meta": {
      "moduleName": "week-09/templates/application.hbs"
    }
  });

  _exports.default = _default;
});
;define("week-09/templates/components/number-game", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "LQZQNvYr",
    "block": "{\"symbols\":[\"&default\"],\"statements\":[[18,1,null],[2,\"\\n\\n\"],[6,[37,6],[[35,7]],null,[[\"default\",\"else\"],[{\"statements\":[[6,[37,6],[[35,5]],null,[[\"default\",\"else\"],[{\"statements\":[[2,\"        \"],[10,\"div\"],[14,0,\"text-center\"],[12],[2,\"Yes! It only took me \"],[1,[34,2]],[2,\" guesses.\"],[13],[2,\"\\n        \"],[10,\"div\"],[14,0,\"text-center\"],[12],[2,\"Do you want to\"],[11,\"button\"],[4,[38,0],[[32,0],\"start\"],null],[12],[2,\"Play again!\"],[13],[13],[2,\"\\n        \"],[10,\"div\"],[14,0,\"text-center\"],[12],[1,[30,[36,4],null,[[\"value\",\"placeholder\"],[[35,3],\"Your Name\"]]]],[11,\"button\"],[4,[38,0],[[32,0],\"save-highscore\"],null],[12],[2,\"Save Highscore!\"],[13],[13],[2,\"\\n\"]],\"parameters\":[]},{\"statements\":[[2,\"    \\n    \"],[10,\"div\"],[14,0,\"text-center\"],[12],[2,\"Is it \"],[1,[34,1]],[2,\"?\"],[13],[2,\"\\n    \"],[10,\"div\"],[14,0,\"text-center\"],[12],[2,\"\\n        \"],[11,\"button\"],[4,[38,0],[[32,0],\"lower\"],null],[12],[2,\"Lower\"],[13],[2,\"\\n        \"],[11,\"button\"],[4,[38,0],[[32,0],\"correct\"],null],[12],[2,\"correct\"],[13],[2,\"\\n        \"],[11,\"button\"],[4,[38,0],[[32,0],\"higher\"],null],[12],[2,\"Higher\"],[13],[2,\"\\n    \"],[13],[2,\"\\n\"]],\"parameters\":[]}]]]],\"parameters\":[]},{\"statements\":[[2,\"    \"],[10,\"div\"],[14,0,\"text-center\"],[12],[2,\"think of a number between 1 and 100 and then press \"],[11,\"button\"],[4,[38,0],[[32,0],\"start\"],null],[12],[2,\"start !\"],[13],[13],[2,\"\\n\"]],\"parameters\":[]}]]]],\"hasEval\":false,\"upvars\":[\"action\",\"guessValue\",\"guesses\",\"player_name\",\"input\",\"correct\",\"if\",\"playing\"]}",
    "meta": {
      "moduleName": "week-09/templates/components/number-game.hbs"
    }
  });

  _exports.default = _default;
});
;define("week-09/templates/components/tic-tac-toe", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "9uLRtrmQ",
    "block": "{\"symbols\":[\"&default\"],\"statements\":[[18,1,null],[2,\"\\n\\n\"],[6,[37,2],[[35,4]],null,[[\"default\",\"else\"],[{\"statements\":[[6,[37,2],[[35,1]],null,[[\"default\"],[{\"statements\":[[2,\"        \"],[10,\"div\"],[12],[2,\"\\n            Player \"],[1,[34,1]],[2,\" won!\\n        \"],[13],[2,\"\\n\"]],\"parameters\":[]}]]],[6,[37,2],[[35,3]],null,[[\"default\"],[{\"statements\":[[2,\"        we'll call it a draw.\\n\"]],\"parameters\":[]}]]],[2,\"    \"],[11,\"button\"],[4,[38,0],[[32,0],\"start\"],null],[12],[2,\"Restart\"],[13],[2,\"\\n\"]],\"parameters\":[]},{\"statements\":[[2,\"    \"],[11,\"button\"],[4,[38,0],[[32,0],\"start\"],null],[12],[2,\"Start\"],[13],[2,\"\\n\"]],\"parameters\":[]}]]],[10,\"canvas\"],[14,1,\"stage\"],[14,\"width\",\"380\"],[14,\"height\",\"380\"],[12],[13]],\"hasEval\":false,\"upvars\":[\"action\",\"winner\",\"if\",\"draw\",\"playing\"]}",
    "meta": {
      "moduleName": "week-09/templates/components/tic-tac-toe.hbs"
    }
  });

  _exports.default = _default;
});
;define("week-09/templates/game", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "3K/z0CoY",
    "block": "{\"symbols\":[],\"statements\":[[1,[34,0]]],\"hasEval\":false,\"upvars\":[\"tic-tac-toe\"]}",
    "meta": {
      "moduleName": "week-09/templates/game.hbs"
    }
  });

  _exports.default = _default;
});
;define("week-09/templates/highscores", ["exports"], function (_exports) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.default = void 0;

  var _default = Ember.HTMLBars.template({
    "id": "2UYL+1lv",
    "block": "{\"symbols\":[\"item\"],\"statements\":[[10,\"div\"],[14,0,\"text-center\"],[12],[2,\"\\n    \"],[10,\"h2\"],[12],[2,\"High-scores\"],[13],[2,\"\\n\"],[13],[2,\"\\n\"],[10,\"ol\"],[12],[2,\"\\n\"],[6,[37,2],[[30,[36,1],[[30,[36,1],[[35,0]],null]],null]],null,[[\"default\"],[{\"statements\":[[2,\"        \"],[10,\"li\"],[12],[2,\"\\n            \"],[10,\"div\"],[14,0,\"float-left\"],[12],[1,[32,1,[\"name\"]]],[13],[2,\"\\n            \"],[10,\"div\"],[14,0,\"float-right\"],[12],[1,[32,1,[\"score\"]]],[13],[2,\"\\n        \"],[13],[2,\"\\n\"]],\"parameters\":[1]}]]],[13]],\"hasEval\":false,\"upvars\":[\"model\",\"-track-array\",\"each\"]}",
    "meta": {
      "moduleName": "week-09/templates/highscores.hbs"
    }
  });

  _exports.default = _default;
});
;define("week-09/transforms/boolean", ["exports", "@ember-data/serializer/-private"], function (_exports, _private) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _private.BooleanTransform;
    }
  });
});
;define("week-09/transforms/date", ["exports", "@ember-data/serializer/-private"], function (_exports, _private) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _private.DateTransform;
    }
  });
});
;define("week-09/transforms/number", ["exports", "@ember-data/serializer/-private"], function (_exports, _private) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _private.NumberTransform;
    }
  });
});
;define("week-09/transforms/string", ["exports", "@ember-data/serializer/-private"], function (_exports, _private) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  Object.defineProperty(_exports, "default", {
    enumerable: true,
    get: function get() {
      return _private.StringTransform;
    }
  });
});
;

;define('week-09/config/environment', [], function() {
  var prefix = 'week-09';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(decodeURIComponent(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

;
          if (!runningTests) {
            require("week-09/app")["default"].create({"name":"week-09","version":"0.0.0+090b6744"});
          }
        
//# sourceMappingURL=week-09.map
